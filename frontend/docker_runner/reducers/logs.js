import { loadLogs } from 'comps/docker_runner/actions'


export const KEY = 'logs'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { logs } = payload || {}
  const { ids, byId } = state

  switch (type) {
    case loadLogs.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case loadLogs.SUCCESS:
      if (!ids.includes(logs.project)) {

        ids.push(logs.project)
      }
      byId[logs.project] = logs
      return {
        ...state,
        ids,
        byId,
      }

    case loadLogs.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectLogs = (state) => state[KEY]
export const selectLogsById = (state, logs) => selectLogs(state).byId[logs.project]
