import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function docker_runnerUri(uri) {
  return v1(`/docker_runner${uri}`)
}

export default class DockerRunner {

  static run() {
    return post(docker_runnerUri(`/run/${storage.getProject()}`), {})
  }


  static loadLogs() {
    return get(docker_runnerUri(`/logs/${storage.getProject()}`))
  }

}
