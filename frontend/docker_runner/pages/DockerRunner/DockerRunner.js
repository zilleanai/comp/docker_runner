import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { v1 } from 'api'
import Helmet from 'react-helmet'
import { Button, ButtonGroup } from 'react-bootstrap';
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { InfoBox, PageContent } from 'components'
import { RunStatus, RunLogs } from 'comps/docker_runner/components'
import { run } from 'comps/docker_runner/actions'

class DockerRunner extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }


  render() {
    const { isLoaded, error, pristine, submitting } = this.props
    if (!isLoaded) {
      return null
    }
    return (
      <PageContent>
        <Helmet>
          <title>Docker Runner</title>
        </Helmet>
        <h1>Docker Runner!</h1>
        <br />
        <div className="row">
          <button type="submit"
            className="button-primary"
            onClick={(e) => {
              this.props.run.trigger({})
            }}
            disabled={pristine || submitting}
          >
            {submitting ? 'Running...' : 'Run'}
          </button>
        </div>
        <RunStatus />
        <RunLogs />
      </PageContent>)
  }
}

const withConnect = connect(
  (state) => {
    const isLoaded = true
    return {
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ run }, dispatch),
)


const withSaga = injectSagas(require('comps/docker_runner/sagas/run'))

export default compose(
  withConnect,
  withSaga,
)(DockerRunner)
