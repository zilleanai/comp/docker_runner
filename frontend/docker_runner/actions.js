import { createRoutine } from 'actions'

export const run = createRoutine('docker_runner/RUN')
export const loadLogs = createRoutine('docker_runner/LOGS')
