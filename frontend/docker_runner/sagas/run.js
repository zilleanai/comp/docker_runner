import { call, put, takeLatest } from 'redux-saga/effects'

import { run } from 'comps/docker_runner/actions'
import { createRoutineFormSaga } from 'sagas'
import DockerRunnerApi from 'comps/docker_runner/api'


export const KEY = 'run'

export const runSaga = createRoutineFormSaga(
  run,
  function* successGenerator(payload) {
    const response = yield call(DockerRunnerApi.run, payload)
    yield put(run.success(response))
  }
)

export default () => [
  takeLatest(run.TRIGGER, runSaga)
]
