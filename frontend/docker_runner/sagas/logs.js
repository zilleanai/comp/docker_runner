import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadLogs } from 'comps/docker_runner/actions'
import DockerRunnerApi from 'comps/docker_runner/api'
import { selectLogs } from 'comps/docker_runner/reducers/logs'


export const KEY = 'logs'

export const maybeLoadLogsSaga = function* ({payload:project}) {
  const { byId, isLoading } = yield select(selectLogs)
  const isLoaded = !!byId[project.project]
  if (!(isLoaded || isLoading)) {
    yield put(loadLogs.trigger())
  }
}

export const loadLogsSaga = createRoutineSaga(
  loadLogs,
  function* successGenerator() {
    const logs = yield call(DockerRunnerApi.loadLogs)
    yield put(loadLogs.success(logs))
  }
)

export default () => [
  takeEvery(loadLogs.MAYBE_TRIGGER, maybeLoadLogsSaga),
  takeLatest(loadLogs.TRIGGER, loadLogsSaga),
]
