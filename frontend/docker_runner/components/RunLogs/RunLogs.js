import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import './run-logs.scss'
import { v1 } from 'api'
import { storage } from 'comps/project'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { loadLogs } from 'comps/docker_runner/actions'
import { selectLogsById } from 'comps/docker_runner/reducers/logs'

class RunLogs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  componentWillMount() {
    const { loadLogs } = this.props
    const project = storage.getProject()
    loadLogs.maybeTrigger({ project })
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      const { loadLogs } = this.props
      const project = storage.getProject()
      loadLogs.maybeTrigger({ project })
      this.setState({ time: Date.now() })
    }, 3000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { isLoaded, logs, error } = this.props
    if (!isLoaded) {
      return null
    }

    return (
      <div className="RunLogs">
        {logs.map((log, i) => {
          return (
            <li key={i} >
              {log}
            </li>
          )
        })}
      </div>
    );
  }
}

const withConnect = connect(
  (state, props) => {
    const project = storage.getProject()
    const logs = selectLogsById(state, project)
    return {
      logs,
      isLoaded: !!logs,
    }
  },
  (dispatch) => bindRoutineCreators({ loadLogs }, dispatch),
)

const withReducer = injectReducer(require('comps/docker_runner/reducers/logs'))
const withSaga = injectSagas(require('comps/docker_runner/sagas/logs'))

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RunLogs)

