# docker_runner

component to run a docker container

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/docker_runner
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.docker_runner',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.docker_runner.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Labeling
} from 'comps/docker_runner/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  DockerRunner: 'DockerRunner',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.DockerRunner,
    path: '/docker_runner',
    component: DockerRunner,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.DockerRunner} />
    ...
</div>
```
