import os
import gc
import json
from io import StringIO, BytesIO
import pickle
import multiprocessing

from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig
import docker


def docker_runner_logger(project, *args, **kwargs):
    redis = AppConfig.SESSION_REDIS
    key = 'docker_runner_logger_'+project
    logged = redis.get(key) or "{\"logs\":[]}"
    logged = json.loads(logged)
    mapped = "".join(map(str, args))
    logged['logs'].append(mapped)
    redis.setex(key, 600, json.dumps(logged))


@celery.task(serializer='pickle')
def docker_runner_async_task(project):
    multiprocessing.current_process()._config['daemon'] = False
    os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
    logger = lambda *args, **kwargs: docker_runner_logger(
        project, args, kwargs)
    client = docker.from_env()
    #image, log = client.images.build('.')
    # logger(log)
    image = 'pytorch/pytorch'
    command = "bash -c 'cd /workspace && pip install -r requirements.txt'"
    volumes = {
        os.getcwd(): {
            'bind': '/workspace', 'mode': 'rw'
        }
    }
    logger(image, command, volumes)
    log = client.containers.run(
        image=image, command=command, volumes=volumes, remove=True)
    logger(log)
