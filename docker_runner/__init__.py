"""
    docker_runner
    ~~~~

    Component for docker_runner in general.

    :copyright: Copyright © 2019 chriamue
    :license: Not open source, see LICENSE for details
"""

__version__ = '0.1.0'


from flask_unchained import Bundle


class DockerRunnerBundle(Bundle):

    name = 'docker_runner_bundle'

    @classmethod
    def before_init_app(cls, app):
        pass

    @classmethod
    def after_init_app(cls, app):
        pass
