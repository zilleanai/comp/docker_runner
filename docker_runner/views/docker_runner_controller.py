import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from werkzeug.utils import secure_filename
from ..tasks import docker_runner_async_task


class DockerRunnerController(Controller):

    @route('/run/<string:project>', methods=['POST'])
    def run(self, project):
        docker_runner_async_task.delay(project)
        return jsonify(success=True)

    @route('/status/<string:project>', methods=['GET'])
    def status(self, project):
        return jsonify(success=True)

    @route('/logs/<string:project>', methods=['GET'])
    def logs(self, project):
        redis = AppConfig.SESSION_REDIS
        key = 'docker_runner_logger_' + project
        logged = redis.get(key) or "{\"logs\":[]}"
        logged = json.loads(logged)
        return jsonify(project=project, logs=logged['logs'])
